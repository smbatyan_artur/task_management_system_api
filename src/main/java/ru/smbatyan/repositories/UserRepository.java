package ru.smbatyan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smbatyan.models.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String userCreatorEmail);
}
