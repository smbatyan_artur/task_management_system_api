package ru.smbatyan.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.smbatyan.models.Task;

import java.util.List;


public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByCreatorId(Long userId, Pageable pageable);

    List<Task> findByCreatorEmail(String userCreatorEmail, Pageable pageable);
}
