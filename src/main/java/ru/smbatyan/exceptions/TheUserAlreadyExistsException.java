package ru.smbatyan.exceptions;

public class TheUserAlreadyExistsException extends RuntimeException {
    public TheUserAlreadyExistsException(String message) {
        super(message);
    }
}
