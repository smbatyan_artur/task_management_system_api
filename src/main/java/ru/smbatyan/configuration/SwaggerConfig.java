package ru.smbatyan.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Task_Management_System_API", version = "0.0.1"))
@SecurityScheme(name = "JWT", scheme = "bearer", type = SecuritySchemeType.HTTP , in = SecuritySchemeIn.HEADER, bearerFormat = "Authorization")
public class SwaggerConfig {
}
