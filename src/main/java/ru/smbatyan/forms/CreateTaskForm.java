package ru.smbatyan.forms;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.smbatyan.models.Task;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class CreateTaskForm {
    @NotBlank(message = "Заголовок не должен быть пустым")
    private String title;
    @NotBlank(message = "Описание не должно быть пустым")
    private String description;
    @NotNull(message = "Установите приоритет задачи")
    private Task.Priority priority;
    private Set<Long> executorsUserId;
}
