package ru.smbatyan.forms;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class CreateCommentTaskForm {
    @NotNull(message = "task_id не должен быть пустым")
    private Long taskId;
    @NotBlank(message = "Контент не должен быть пустым")
    private String content;
}
