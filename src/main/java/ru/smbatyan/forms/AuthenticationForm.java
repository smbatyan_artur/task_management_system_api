package ru.smbatyan.forms;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import ru.smbatyan.validators.Email;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class AuthenticationForm {
    @Email
    private String email;
    @NotBlank(message = "Пароль не должен быть пустым")
    private String password;
}
