package ru.smbatyan.forms;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import ru.smbatyan.models.Task;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class UpdateStatusTaskForm {
    @NotNull(message = "task_id не должен быть пустым")
    private Long taskId;
    @NotNull(message = "Status не должен быть пустым")
    private Task.Status status;
}
