package ru.smbatyan.services;

import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.forms.RegistrationForm;

@Validated
public interface UserRegistrationService {

    void register(@Valid RegistrationForm registrationForm);
}
