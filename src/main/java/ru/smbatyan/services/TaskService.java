package ru.smbatyan.services;

import jakarta.validation.Valid;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.dto.TaskDto;
import ru.smbatyan.forms.CreateCommentTaskForm;
import ru.smbatyan.forms.CreateTaskForm;
import ru.smbatyan.forms.UpdateStatusTaskForm;
import ru.smbatyan.validators.Email;

import java.util.Set;

@Validated
public interface TaskService {

    TaskDto create(@Valid CreateTaskForm createTaskForm, @Email String userCreatorEmail);

    TaskDto delete(@Valid @NotNull Long taskId, @Email String userCreatorEmail);

    Set<TaskDto> getUserTasks(@Valid @Min(0) int page, @Min(20) int size, @NotNull(message = "user_id не должен быть пустым") Long userId);

    Set<TaskDto> getAListOfYourOwnTasks(@Valid @Min(0) int page, @Min(20) int size, @Email String userCreatorEmail);

    Set<TaskDto> getAListOfTasksAssignedToMe(@Valid @Min(0) int page, @Min(20) int size, @Email String userCreatorEmail);

    TaskDto createCommentOnATask(@Valid CreateCommentTaskForm createCommentTaskForm, @Email String userCreatorEmail);

    TaskDto updateStatusTask(@Valid UpdateStatusTaskForm updateStatusTaskForm, @Email String userCreatorEmail);
}
