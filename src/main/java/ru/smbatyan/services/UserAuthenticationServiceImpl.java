package ru.smbatyan.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.smbatyan.dto.TokenDto;
import ru.smbatyan.exceptions.UserNotFoundException;
import ru.smbatyan.forms.AuthenticationForm;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.UserRepository;

import java.util.Optional;

@AllArgsConstructor
@Service
public class UserAuthenticationServiceImpl implements UserAuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final Algorithm algorithm;

    @Override
    public TokenDto authenticate(AuthenticationForm authenticationForm) {
        final Optional<User> optionalUser = userRepository.findByEmail(authenticationForm.getEmail());
        if (optionalUser.isEmpty() || !passwordEncoder.matches(authenticationForm.getPassword(), optionalUser.get().getPassword())) {
            throw new UserNotFoundException("Пользователь не найден");
        }
        final String token = JWT.create()
                .withClaim("email", authenticationForm.getEmail())
                .sign(algorithm);
        return TokenDto.builder().type("bearer").token(token).build();
    }

}
