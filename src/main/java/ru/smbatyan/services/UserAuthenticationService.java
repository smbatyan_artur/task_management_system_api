package ru.smbatyan.services;

import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import ru.smbatyan.dto.TokenDto;
import ru.smbatyan.forms.AuthenticationForm;

@Validated
public interface UserAuthenticationService {
    TokenDto authenticate(@Valid AuthenticationForm authenticationForm);

}
