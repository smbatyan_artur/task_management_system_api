package ru.smbatyan.services;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smbatyan.dto.TaskDto;
import ru.smbatyan.exceptions.AccessDenialException;
import ru.smbatyan.exceptions.TaskNotFoundException;
import ru.smbatyan.exceptions.UserNotFoundException;
import ru.smbatyan.forms.CreateCommentTaskForm;
import ru.smbatyan.forms.CreateTaskForm;
import ru.smbatyan.forms.UpdateStatusTaskForm;
import ru.smbatyan.models.Comment;
import ru.smbatyan.models.Task;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.TaskRepository;
import ru.smbatyan.repositories.UserRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;


    @Override
    public TaskDto create(CreateTaskForm createTaskForm, String userCreatorEmail) {
        final Task task = Task.builder().build();
        task.setCreator(findByEmail(userCreatorEmail));
        task.setTitle(createTaskForm.getTitle());
        task.setDescription(createTaskForm.getDescription());
        task.setPriority(createTaskForm.getPriority());
        task.setStatus(Task.Status.IN_WAITING);

        if (createTaskForm.getExecutorsUserId() != null) {
            final Set<User> executors = new HashSet<>();
            createTaskForm.getExecutorsUserId().forEach(userId -> executors.add(findById(userId)));
            task.setExecutors(executors);
        }

        taskRepository.saveAndFlush(task);

        return modelMapper.map(task, TaskDto.class);
    }


    @Override
    public TaskDto delete(Long taskId, String userCreatorEmail) {
        final Optional<Task> optionalTask = taskRepository.findById(taskId);
        if (optionalTask.isPresent()) {
            if (!optionalTask.get().getCreator().getEmail().equals(userCreatorEmail))
                throw new AccessDenialException("Вы не можете удалить данную задачу");
            taskRepository.delete(optionalTask.get());
            return modelMapper.map(optionalTask.get(), TaskDto.class);
        } else throw new TaskNotFoundException("Здача с id -> " + taskId + " не найдена");
    }


    @Override
    public Set<TaskDto> getUserTasks(int page, int size, Long userId) {
        final Set<TaskDto> result = new HashSet<>();
        taskRepository.findByCreatorId(userId, PageRequest.of(page, size)).forEach(task -> {
            result.add(modelMapper.map(task, TaskDto.class));
        });
        return result;
    }

    @Override
    public Set<TaskDto> getAListOfYourOwnTasks(int page, int size, String userCreatorEmail) {
        final Set<TaskDto> result = new HashSet<>();
        taskRepository.findByCreatorEmail(userCreatorEmail, PageRequest.of(page, size)).forEach(task -> {
            result.add(modelMapper.map(task, TaskDto.class));
        });
        return result;
    }


    @Override
    public Set<TaskDto> getAListOfTasksAssignedToMe(int page, int size, String userCreatorEmail) {
        final Set<TaskDto> result = new HashSet<>();
        userRepository.findByEmail(userCreatorEmail).ifPresent(user ->
                user.getTasksToBeCompleted().forEach(task -> result.add(modelMapper.map(task, TaskDto.class))));
        return result;
    }


    @Transactional
    @Override
    public TaskDto createCommentOnATask(CreateCommentTaskForm createCommentTaskForm, String userCreatorEmail) {
        Optional<Task> optionalTask = taskRepository.findById(createCommentTaskForm.getTaskId());
        if (optionalTask.isPresent()) {
            optionalTask.get().getComments().add(Comment.builder()
                    .task(optionalTask.get())
                    .creator(findByEmail(userCreatorEmail))
                    .content(createCommentTaskForm.getContent())
                    .build());
            return modelMapper.map(taskRepository.saveAndFlush(optionalTask.get()), TaskDto.class);
        }
        throw new TaskNotFoundException("Здача с id -> " + createCommentTaskForm.getTaskId() + " не найдена");
    }


    @Transactional
    @Override
    public TaskDto updateStatusTask(UpdateStatusTaskForm updateStatusTaskForm, String userCreatorEmail) {
        final Optional<Task> optionalTask = taskRepository.findById(updateStatusTaskForm.getTaskId());
        boolean flag = false;
        if (optionalTask.isPresent()) {
            optionalTask.get().setStatus(updateStatusTaskForm.getStatus());

            for (User user : optionalTask.get().getExecutors())
                if (user.getEmail().equals(userCreatorEmail)) {
                    flag = true;
                    break;
                }

            if (optionalTask.get().getCreator().getEmail().equals(userCreatorEmail)) {
                flag = true;
            }

            if (flag) {
                return modelMapper.map(taskRepository.saveAndFlush(optionalTask.get()), TaskDto.class);
            } else {
                throw new AccessDenialException("Вы не можете изменить статут задачи по id -> " + updateStatusTaskForm.getTaskId());
            }
        }
        throw new TaskNotFoundException("Здача с id -> " + updateStatusTaskForm.getTaskId() + " не найдена");
    }


    private User findByEmail(String userEmail) {
        final Optional<User> optionalUser = userRepository.findByEmail(userEmail);
        if (optionalUser.isPresent())
            return optionalUser.get();
        else throw new UserNotFoundException("Пользователь c email -> " + userEmail + " не найден");
    }


    private User findById(Long userId) {
        final Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent())
            return optionalUser.get();
        else throw new UserNotFoundException("Пользователь c id -> " + userId + " не найден");
    }
}
