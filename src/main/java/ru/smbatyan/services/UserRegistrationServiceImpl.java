package ru.smbatyan.services;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.smbatyan.exceptions.TheUserAlreadyExistsException;
import ru.smbatyan.forms.RegistrationForm;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.UserRepository;

@AllArgsConstructor
@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void register(RegistrationForm registrationForm) {
        if (userRepository.findByEmail(registrationForm.getEmail()).isEmpty()) {
            userRepository.saveAndFlush(User.builder()
                    .email(registrationForm.getEmail())
                    .password(passwordEncoder.encode(registrationForm.getPassword()))
                    .build());
        } else
            throw new TheUserAlreadyExistsException("Пользователь с таким email -> " + registrationForm.getEmail() + " уже существует.");
    }



}
