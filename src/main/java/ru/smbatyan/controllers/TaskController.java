package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.smbatyan.dto.TaskDto;
import ru.smbatyan.forms.CreateCommentTaskForm;
import ru.smbatyan.forms.CreateTaskForm;
import ru.smbatyan.forms.UpdateStatusTaskForm;
import ru.smbatyan.services.TaskService;

import java.security.Principal;
import java.util.Set;

@Tag(name = "Управление задачами")
@SecurityRequirement(name = "JWT")
@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/task")
public class TaskController {

    private final TaskService taskService;

    @Operation(summary = "Создать задачу")
    @PostMapping("/create")
    public ResponseEntity<TaskDto> create(@RequestBody CreateTaskForm createTaskForm, Principal principal) {
        return ResponseEntity.status(201).body(taskService.create(createTaskForm, principal.getName()));
    }


    @Operation(summary = "Удалить задачу")
    @DeleteMapping("/delete")
    public ResponseEntity<TaskDto> delete(@RequestParam(name = "task_id") Long taskId, Principal principal) {
        return ResponseEntity.status(200).body(taskService.delete(taskId, principal.getName()));
    }


    @Operation(summary = "Получить все задачи пользователя по его id")
    @GetMapping("/list")
    public ResponseEntity<Set<TaskDto>> getUserTasks(@RequestParam(value = "page", defaultValue = "0") int page,
                                                     @RequestParam(value = "size", defaultValue = "20") int size,
                                                     @RequestParam(value = "creator_id") Long creatorId) {
        return ResponseEntity.ok(taskService.getUserTasks(page, size, creatorId));
    }

    @Operation(summary = "Получить список своих созданных задач")
    @GetMapping("/list/my")
    public ResponseEntity<Set<TaskDto>> getAListOfYourOwnTasks(@RequestParam(value = "page", defaultValue = "0") int page,
                                                               @RequestParam(value = "size", defaultValue = "20") int size,
                                                               Principal principal) {
        return ResponseEntity.ok(taskService.getAListOfYourOwnTasks(page, size, principal.getName()));
    }

    @Operation(summary = "Получите список порученных мне задач")
    @GetMapping("/list/to_me")
    public ResponseEntity<Set<TaskDto>> getAListOfTasksAssignedToMe(@RequestParam(value = "page", defaultValue = "0") int page,
                                                                    @RequestParam(value = "size", defaultValue = "20") int size,
                                                                    Principal principal) {
        return ResponseEntity.ok(taskService.getAListOfTasksAssignedToMe(page, size, principal.getName()));
    }

    @Operation(summary = "Создать комментарий к задаче")
    @PostMapping("/create/comment")
    public ResponseEntity<TaskDto> createCommentOnATask(@RequestBody CreateCommentTaskForm createCommentTaskForm, Principal principal) {
        return ResponseEntity.status(201).body(taskService.createCommentOnATask(createCommentTaskForm, principal.getName()));
    }

    @Operation(summary = "Обновить статус задачи")
    @PatchMapping("/update/status")
    public ResponseEntity<TaskDto> updateStatusTask(@RequestBody UpdateStatusTaskForm updateStatusTaskForm, Principal principal) {
        return ResponseEntity.ok(taskService.updateStatusTask(updateStatusTaskForm, principal.getName()));
    }
}
