package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.smbatyan.forms.RegistrationForm;
import ru.smbatyan.services.UserRegistrationService;

@Tag(name = "Регестрация пользователей")
@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class UserRegistrationController {

    private final UserRegistrationService userRegistrationService;

    @Operation(summary = "Регистрация пользователя")
    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody RegistrationForm registrationForm){
        userRegistrationService.register(registrationForm);
        return ResponseEntity.status(201).build();
    }
}
