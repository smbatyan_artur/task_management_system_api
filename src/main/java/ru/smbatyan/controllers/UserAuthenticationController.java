package ru.smbatyan.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.smbatyan.dto.TokenDto;
import ru.smbatyan.forms.AuthenticationForm;
import ru.smbatyan.services.UserAuthenticationService;

@Tag(name = "Аутентификация пользователей")
@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class UserAuthenticationController {

    private final UserAuthenticationService userAuthenticationService;

    @Operation(summary = "Аутентификация пользователя")
    @PostMapping("/authenticate")
    public ResponseEntity<TokenDto> authenticate(@RequestBody AuthenticationForm authenticationForm){
        return ResponseEntity.ok(userAuthenticationService.authenticate(authenticationForm));
    }
}
