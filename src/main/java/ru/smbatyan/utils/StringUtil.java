package ru.smbatyan.utils;

public class StringUtil {
    public static String getString(int start, String str) {
        char[] c = str.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = start; i < c.length; i++) {
            result.append(c[i]);
        }
        return result.toString();
    }
}
