package ru.smbatyan.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.smbatyan.models.Task;

import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class TaskDto {
    private Long id;
    private String title;
    private String description;
    private Task.Priority priority;
    private CreatorDto creator;
    private Task.Status status;
    private Set<ExecutorDto> executors;
    private Set<CommentDto> comments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDto taskDto = (TaskDto) o;
        return Objects.equals(id, taskDto.id) && Objects.equals(title, taskDto.title) && Objects.equals(description, taskDto.description) && priority == taskDto.priority && Objects.equals(creator, taskDto.creator) && status == taskDto.status && Objects.equals(executors, taskDto.executors) && Objects.equals(comments, taskDto.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, priority, creator, status, executors, comments);
    }
}
