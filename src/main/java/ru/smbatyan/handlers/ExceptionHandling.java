package ru.smbatyan.handlers;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.smbatyan.exceptions.AccessDenialException;
import ru.smbatyan.exceptions.TaskNotFoundException;
import ru.smbatyan.exceptions.TheUserAlreadyExistsException;
import ru.smbatyan.exceptions.UserNotFoundException;

@ControllerAdvice
public class ExceptionHandling {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e){
        return ResponseEntity.status(400).body("Message: " + e.getMessage());
    }

    @ExceptionHandler(TheUserAlreadyExistsException.class)
    public ResponseEntity<String> handleTheUserAlreadyExistsException(TheUserAlreadyExistsException e){
        return ResponseEntity.status(403).body("Message: " + e.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException e){
        return ResponseEntity.status(404).body("Message: " + e.getMessage());
    }

    @ExceptionHandler(TaskNotFoundException.class)
    public ResponseEntity<String> handleTaskNotFoundException(TaskNotFoundException e){
        return ResponseEntity.status(404).body("Message: " + e.getMessage());
    }

    @ExceptionHandler(AccessDenialException.class)
    public ResponseEntity<String> handleAccessDenialException(AccessDenialException e){
        return ResponseEntity.status(403).body("Message: " + e.getMessage());
    }
}
