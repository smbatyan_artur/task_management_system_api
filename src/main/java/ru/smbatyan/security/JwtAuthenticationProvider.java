package ru.smbatyan.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.smbatyan.utils.StringUtil;

public class JwtAuthenticationProvider {
    private final Algorithm algorithm;

    public JwtAuthenticationProvider(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public void authenticationByToken(String token) {

        DecodedJWT jwt;

        try {
            jwt = JWT.require(algorithm)
                    .build()
                    .verify(StringUtil.getString(7, token));
        } catch (JWTVerificationException e) {
            return;
        }

        Authentication authentication = new Authentication();
        authentication.setAuthenticated(true);
        authentication.setUsername(jwt.getClaim("email").asString());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
