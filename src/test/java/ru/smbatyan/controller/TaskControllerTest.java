package ru.smbatyan.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.smbatyan.dto.TokenDto;
import ru.smbatyan.forms.AuthenticationForm;
import ru.smbatyan.forms.CreateTaskForm;
import ru.smbatyan.models.Task;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.TaskRepository;
import ru.smbatyan.repositories.UserRepository;
import ru.smbatyan.services.UserAuthenticationService;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TaskControllerTest extends BasicControllerTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserAuthenticationService userAuthenticationService;
    @Autowired
    private TaskRepository taskRepository;

    private final static String URL = "/api/v1/task";


    @AfterEach
    public void clearTables() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
    }


    @Transactional
    @Test
    public void isOkCreateTask() throws Exception {

        userRepository.saveAndFlush(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        User executor = userRepository.saveAndFlush(User.builder()
                .email("admin2@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        TokenDto authenticate = userAuthenticationService.authenticate(AuthenticationForm.builder()
                .email("admin@inbox.ru")
                .password("admin")
                .build());

        CreateTaskForm createTaskForm = CreateTaskForm.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .executorsUserId(Set.of(executor.getId()))
                .build();

        mockMvc.perform(post(URL + "/create")
                        .header("Authorization", authenticate.getType() + " " + authenticate.getToken())
                        .content(objectMapper.writeValueAsString(createTaskForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.title").value("String"))
                .andExpect(jsonPath("$.description").value("String"))
                .andExpect(jsonPath("$.priority").value("HIGH"))
                .andExpect(jsonPath("$.creator.email").value("admin@inbox.ru"))
                .andExpect(jsonPath("$.executors.[0].email").value("admin2@inbox.ru"));
    }


    @Test
    public void isOkCreateTaskWithoutExecutors() throws Exception {

        userRepository.saveAndFlush(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        TokenDto authenticate = userAuthenticationService.authenticate(AuthenticationForm.builder()
                .email("admin@inbox.ru")
                .password("admin")
                .build());

        CreateTaskForm createTaskForm = CreateTaskForm.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .build();

        mockMvc.perform(post(URL + "/create")
                        .header("Authorization", authenticate.getType() + " " + authenticate.getToken())
                        .content(objectMapper.writeValueAsString(createTaskForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.title").value("String"))
                .andExpect(jsonPath("$.description").value("String"))
                .andExpect(jsonPath("$.priority").value("HIGH"))
                .andExpect(jsonPath("$.creator.email").value("admin@inbox.ru"))
                .andExpect(jsonPath("$.executors").isEmpty());
    }


    @Test
    public void notAuthorized() throws Exception {

        CreateTaskForm createTaskForm = CreateTaskForm.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .executorsUserId(Set.of(1L))
                .build();

        mockMvc.perform(post(URL + "/create")
                        .content(objectMapper.writeValueAsString(createTaskForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }


    @Test
    public void NonExistingExecutor() throws Exception {

        userRepository.saveAndFlush(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        TokenDto authenticate = userAuthenticationService.authenticate(AuthenticationForm.builder()
                .email("admin@inbox.ru")
                .password("admin")
                .build());

        CreateTaskForm createTaskForm = CreateTaskForm.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .executorsUserId(Set.of(1L))
                .build();

        mockMvc.perform(post(URL + "/create")
                        .header("Authorization", authenticate.getType() + " " + authenticate.getToken())
                        .content(objectMapper.writeValueAsString(createTaskForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }


    @Transactional
    @Test
    public void isOkDeleteTask() throws Exception {

        User creator = userRepository.save(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        User executor = userRepository.save(User.builder()
                .email("admin2@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        Task task = Task.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .creator(creator)
                .status(Task.Status.IN_WAITING)
                .executors(Set.of(executor))
                .build();

        taskRepository.saveAndFlush(task);

        TokenDto authenticate = userAuthenticationService.authenticate(AuthenticationForm.builder()
                .email("admin@inbox.ru")
                .password("admin")
                .build());

        mockMvc.perform(delete(URL + "/delete")
                        .header("Authorization", authenticate.getType() + " " + authenticate.getToken())
                        .param("task_id", task.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.title").value("String"))
                .andExpect(jsonPath("$.description").value("String"))
                .andExpect(jsonPath("$.priority").value("HIGH"))
                .andExpect(jsonPath("$.status").value("IN_WAITING"))
                .andExpect(jsonPath("$.creator.email").value("admin@inbox.ru"))
                .andExpect(jsonPath("$.executors.[0].email").value("admin2@inbox.ru"));
    }


    @Transactional
    @Test
    public void isNoAccessToDeleteTask() throws Exception {

        User creator = userRepository.save(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        User executor = userRepository.save(User.builder()
                .email("admin2@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        Task task = Task.builder()
                .title("String")
                .description("String")
                .priority(Task.Priority.HIGH)
                .creator(creator)
                .status(Task.Status.IN_WAITING)
                .executors(Set.of(executor))
                .build();

        taskRepository.saveAndFlush(task);

        TokenDto authenticate = userAuthenticationService.authenticate(AuthenticationForm.builder()
                .email("admin2@inbox.ru")
                .password("admin")
                .build());

        mockMvc.perform(delete(URL + "/delete")
                        .header("Authorization", authenticate.getType() + " " + authenticate.getToken())
                        .param("task_id", task.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }
}
