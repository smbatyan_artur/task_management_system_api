package ru.smbatyan.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import ru.smbatyan.forms.AuthenticationForm;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class UserAuthenticationControllerTest extends BasicControllerTest{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private final static String URL ="/api/v1/authenticate";


    @AfterEach
    public void clearTables() {
        userRepository.deleteAll();
    }

    @Test
    public void isOkAuthenticate() throws Exception {

        userRepository.saveAndFlush(User.builder()
                .email("admin@inbox.ru")
                .password(passwordEncoder.encode("admin"))
                .build());

        AuthenticationForm authenticationForm = AuthenticationForm.builder()
                .email("admin@inbox.ru")
                .password("admin")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(authenticationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.type").value("bearer"))
                .andExpect(jsonPath("$.token").isString());
    }


    @Test
    public void userNotFound() throws Exception{

        AuthenticationForm authenticationForm = AuthenticationForm.builder()
                .email("admin@inox.ru")
                .password("admin")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(authenticationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }


    @Test
    public void incorrectEmail() throws Exception {

        AuthenticationForm authenticationForm = AuthenticationForm.builder()
                .password("admin")
                .email("aaa.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(authenticationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }


    @Test
    public void emptyPassword() throws Exception {

        AuthenticationForm authenticationForm = AuthenticationForm.builder()
                .password("")
                .email("admin@inbox.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(authenticationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

}
