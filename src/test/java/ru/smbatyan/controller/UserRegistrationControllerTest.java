package ru.smbatyan.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.smbatyan.forms.RegistrationForm;
import ru.smbatyan.models.User;
import ru.smbatyan.repositories.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserRegistrationControllerTest extends BasicControllerTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    private final static String URL ="/api/v1/register";

    @AfterEach
    public void clearTables() {
        userRepository.deleteAll();
    }

    @Test
    public void isOkRegistration() throws Exception {

        RegistrationForm registrationForm = RegistrationForm.builder()
                .password("admin")
                .email("admin@inbox.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(registrationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201));
    }


    @Test
    public void usersEmailConflict() throws Exception {

        userRepository.saveAndFlush(User.builder().email("admin@inbox.ru").password("admin").build());

        RegistrationForm registrationForm = RegistrationForm.builder()
                .password("admin")
                .email("admin@inbox.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(registrationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }


    @Test
    public void incorrectEmail() throws Exception {

        RegistrationForm registrationForm = RegistrationForm.builder()
                .password("admin")
                .email("aaa.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(registrationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }


    @Test
    public void emptyPassword() throws Exception {

        RegistrationForm registrationForm = RegistrationForm.builder()
                .password("")
                .email("admin@inbox.ru")
                .build();

        mockMvc.perform(post(URL)
                        .content(objectMapper.writeValueAsString(registrationForm))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }
}
